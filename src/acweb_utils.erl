%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. Apr 2018 6:18 AM
%%%-------------------------------------------------------------------
-module(acweb_utils).
-author("adean").

-export([json_to_map/1,find/2, find_value/2, to_json/1, to_binary/1]).
-define(BQUOTE(V), [<<"\"">>, V, <<"\"">>]).
%%-export([to_binary/1, str_to_number/1]).


%%cast_on_interval(Interval, Pid, Msg) ->
%%    receive
%%      _ -> []
%%    after Interval ->
%%      gen_server:cast(Pid, Msg)
%%    end,
%%  cast_on_interval(Interval, Pid, Msg).

json_to_map(Bin) ->
  try rfc4627:decode(Bin) of
    {ok, {_, Json}, _} ->
      {ok, maps:from_list(Json)};
    Error ->
      lager:error("Error Decoding Json from ~s with Error: ~p", [Bin, Error]),
      {error, []}
  catch
    Exception:Reason ->
      lager:error("Json Conversion Error: ~p - ~p", [Exception, Reason]),
      {error, []}
  end.

to_json(Map) when is_map(Map) ->
  binary:list_to_bin(
    [<<"{">>,
      to_json(lists:sort(fun(A, B) -> A > B end, maps:to_list(Map))),
      <<"}">>]
  );

to_json([{K, V}]) -> [to_binary(K), <<":">>, to_json(V)];
to_json([{K, V} | Rest]) -> [to_json(Rest), <<",">> | [to_binary(K), <<":">>, to_json(V)]];
to_json(V = [C | _]) when not is_list(C) -> to_binary(V);
to_json(V) -> to_binary(V).

to_binary(V) when is_integer(V) -> erlang:integer_to_binary(V);
to_binary(V) when is_float(V) -> erlang:float_to_binary(V, [{decimals, 2}]);
to_binary(V = [C | _]) when is_list(V) andalso not is_list(C) andalso not is_tuple(C)->
  ?BQUOTE(binary:list_to_bin(V));   %make this is a string not another list.
to_binary(V) when is_binary(V) -> ?BQUOTE(V);
to_binary(V) when is_atom(V) -> ?BQUOTE(erlang:atom_to_binary(V, utf8)).

%%extract_numbers(Str) when is_list(Str) ->
%%  [X || X <- Str, (X > 46) and (X < 58)];
%%
%%extract_numbers(Str) when is_binary(Str) ->
%%  list_to_binary([X || <<X>> <= Str, (X > 46) and (X < 58)]).

find(Func, List) ->
  try lists:foreach(
    fun(L) ->
      case Func(L) of
        false -> [];
        true -> throw({ok, L})
      end
    end, List) of
    _ -> {not_found, []}
  catch
    {ok, Result} -> {ok, Result}
  end.

% the Func should return {true, Value} or {false, []}
find_value(Func, List) ->
  try lists:foreach(
    fun(L) ->
      case Func(L) of
        {false, _} -> [];
        {true, V} -> throw({ok, V});
        Msg -> lager:error("Bad Case clause ~p", [Msg])
      end
    end, List) of
    _ -> {not_found, []}
  catch
      Result -> Result
  end.

%%-spec eval_until({module(), func(), list()}, list(), boolean()) -> boolean().
%%eval_until({M, F, A}, List, Cond) ->
%%  try lists:foreach(
%%    fun(L) ->
%%      case apply(M, F, A) of
%%        Cond -> throw(Cond);
%%        _    -> not Cond
%%      end
%%    end, List) of
%%    _ -> not Cond
%%  catch
%%      Result -> Result
%%  end.

%%to_binary(S) when is_list(S) -> list_to_binary(S);
%%to_binary(S) when is_integer(S) -> integer_to_binary(S);
%%to_binary(S) when is_atom(S) -> atom_to_binary(S, latin1);
%%to_binary(S) when is_binary(S) -> S.
%%
%%str_to_number(S) when is_binary(S) -> binary_to_integer(S);
%%str_to_number(S) when is_list(S) -> list_to_integer(S).


%%ip_str_to_tuple(V) when is_binary(V) ->
%%  try list_to_tuple([binary_to_integer(X) || X <- binary:split(V, <<".">>, [global])]) of
%%    {_, _, _, _} = Ip -> Ip;
%%    _ -> V
%%  catch
%%    _ -> V
%%  end.

%%tuple_to_ip_str({A, B, C, D}) -> io_lib:format("~w.~w.~w.~w", [A, B, C, D]).


