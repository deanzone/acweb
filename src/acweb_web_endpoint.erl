%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 15. Apr 2018 2:00 PM
%%%-------------------------------------------------------------------
-module(acweb_web_endpoint).
-author("adean").

%% API
-export([start_link/0]).

start_link() ->
  Port = application:get_env(acweb, web_port, 4000),
  SseRoutes = application:get_env(acweb, sse_routes, []),
  Dispatch = cowboy_router:compile([
    {'_',
      SseRoutes ++
      [
        {"/[...]", acweb_web_router, get_init_state()}
      ]
    }
  ]),
  lager:info("Starting Http Listener..."),
  {ok, _} = cowboy:start_clear(http_listener,
    [{port, Port}],
    #{env => #{dispatch => Dispatch}}
  ),
  lager:info("Listening on port ~p", [Port]),
  self().


get_init_state() ->
  AuthMods =
    lists:map(fun({Name, Module, Func}) -> {Name, {Module, Func}} end,
        application:get_env(acweb, auth_modules, [])),
  #{auth_mods => AuthMods}.